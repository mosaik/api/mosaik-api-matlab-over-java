package de.offis.mosaik.api.matlab;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.offis.mosaik.api.MosaikProxy;
import de.offis.mosaik.api.SimProcess;
import de.offis.mosaik.api.Simulator;
import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

public class MatlabSimulator extends Simulator{
    private static final Logger logger = LoggerFactory.getLogger("Simulator");
	
	private MatlabProxy matlab;
	private String simName;
	@SuppressWarnings("unused")
	private static JSONObject meta = new JSONObject();
	
    /**
     * Is directly called by mosaik as specified in sim_config.
     * For example'java -jar MatSim.jar %(addr)s Zaehlersimulator'
     * @param args - args[0] is the Socket address, args[1] is the simulator
     * name
     */
	public static void main(String[] args) throws Throwable {
		logger.info("Java: MATLAB-API starting on connection "+args[0]+"\n");		
		Simulator simulator = new MatlabSimulator(args[1]);		
		SimProcess.startSimulation(args, simulator);		
	}
	
    /**
     * Creates a new simulator instance.
     * Also creates the Connection to matlab.
     * @param simName is the simulation's name.
     */
    public MatlabSimulator(String simName) throws MatlabConnectionException, MatlabInvocationException {
        super(simName); //Invoke parent constructor
        logger.debug("Starting MatlabSimulator " + simName);
        this.simName = simName;        
        
		// Create Matlab instance
		MatlabProxyFactoryOptions options = new MatlabProxyFactoryOptions.Builder()
				.setUsePreviouslyControlledSession(true).setHidden(true)
				.build();
		MatlabProxyFactory factory = new MatlabProxyFactory(options);
		logger.debug("Got MatlabProxyFactory");
		
		this.matlab = factory.getProxy();
		logger.debug("Got MatlabProxy");
		
		// Go to current directory
		matlab.eval("cd "+ System.getProperty("user.dir"));
		// Invoke Matlab constructor
		this.matlab.eval("MATS"+simName+" = "+simName+"('MATS"+simName+"');");

		
		logger.info("Java: Constructor Executed. New " +simName+" created in MATLAB.\n");
    }
    
    /**
     * Initialize the simulator with the ID <em>sid</em> and apply additional
     * parameters <em>(simParams)</em> sent by mosaik.
     *
     * @param sid is the ID mosaik has given to this simulator.
     * @param simParams a map with additional simulation parameters.
     * @return the meta data dictionary (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#init}).
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	@Override
    public Map<String, Object> init(String sid, Map<String, Object>
            simParams) throws MatlabConnectionException, MatlabInvocationException {
    	logger.debug("Java init function called.");
    	//Call Matlab function
    	Object[] meta_recieved = this.matlab.returningEval("MATS"+this.simName + ".init('"+simParams+"','"+ sid +"')",1);
    	
    	//Object[]->String->JSONValue->JSONObject
    	JSONObject meta = (JSONObject)JSONValue.parse((String)meta_recieved[0]);
    	MatlabSimulator.meta = meta;
    	
    	logger.debug("Meta = \n"+meta);
    	logger.debug("Java init function executed.");
    	return meta;
    }


    /**
     * Create <em>num</em> instances of <em>model</em> using the provided
     * <em>model_params</em>.
     *
     * @param num is the number of instances to create.
     * @param model is the name of the model to instantiate. It needs to be
     *              listed in the simulator's meta data and be public.
     * @param modelParams is a map containing additional model parameters.
     * @return a (nested) list of maps describing the created entities (model
     *         instances) (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#create}).
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	@Override
    public  List<Map<String, Object>> create(int num, String model,
            Map<String, Object> modelParams) throws MatlabConnectionException, MatlabInvocationException
    {
    	Instant start = Instant.now();
    	logger.info("Java create function called for simulator " + simName);
    	logger.debug("modelParams: " + modelParams);
    	
    	//Call Matlab function
    	Object[] created_recieved = this.matlab.returningEval("MATS"+this.simName + ".create("+num+",'"+model+"','"+modelParams+"')",1);
  
    	//Extract returned JSONArray
    	JSONArray created = (JSONArray)JSONValue.parse((String)created_recieved[0]);
    	
    	Instant end = Instant.now();
    	Duration duration = Duration.between(start, end);
    	logger.info("created instances of simulator " + simName + " in " + duration.toHours() + ":" + duration.toMinutes() % 60 + " hours");
    	logger.debug("created: " + created);
    	return created;
    }

    /**
     * Perform the next simulation step from time <em>time</em> using input
     * values from <em>inputs</em> and return the new simulation time (the time
     * at which <code>step()</code> should be called again).
     *
     * @param time is the current time in seconds from simulation start.
     * 			I could not find a way to retrieve the "long"-type from matlab. \R
     * @param inputs is a map of input values (see {@link
     *               https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#step}).
     * @return the time at which this method should be called again in seconds
     *         since simulation start. Only double precision actually given.
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
	public long step(long time, Map<String, Object> inputs)
            throws Exception { 
    	Instant start = Instant.now();
    	logger.info("step function called for " + this.simName + " (time: " + time + ")");
    	
    	boolean foundrequest = false;
    	Map<String, Object> thisModelInputs = new JSONObject();
    	HashMap<String, Object> inputsCopy = new JSONObject();
    	boolean call_again = true;
    	long new_time = -1;
    	long recieved_time = -1;
    	int iteration = 1;
    	while(call_again)
    	{
    		logger.debug("Java Step loop iteration: "+iteration);
    		logger.debug("Input is: "+inputs);    		
    		
    		// ----  Call step a first time and unravel the returned answers  ---------
    	   	Object[] recieved = this.matlab.returningEval("MATS"+this.simName + ".step("+time+",'"+inputs+"',"+iteration+")",1);
    	   	logger.debug("Recieved: " + recieved); //something like: [Ljava.lang.Object;@29774679
    	    JSONObject answer = (JSONObject) JSONValue.parse((String)recieved[0]);
    	   	JSONObject return_struct = (JSONObject) answer.get("return_struct");
    	   	logger.debug("Recieved: " + return_struct.keySet());
    	   	logger.debug("Recieved: " + return_struct);
	    	recieved_time = (long) return_struct.get("recall_time");
	    	if ((long) return_struct.get("call_sim_again") == 1){
	    		call_again = true;}
	    	else{
	    		call_again = false;
	    	}
	    	
	    	JSONObject requests = null;
	    	Object simReq = return_struct.get("SimRequests");
	    	if(simReq instanceof org.json.simple.JSONObject) {
	    		requests = (JSONObject) return_struct.get("SimRequests");
//	    		logger.debug("JSONObject: " + requests);
	    		if (requests.isEmpty()){continue;}; // break this iteration if there are no requests
	    	} else if(simReq instanceof org.json.simple.JSONArray) {
	    		JSONArray requestsArray = (JSONArray) return_struct.get("SimRequests");
//	    		logger.debug("JSONArray: " + requestsArray);
	    		if (requestsArray.isEmpty()){
	    			continue;// break this iteration if there are no requests
	    		} else {
	    			logger.error("SimRequests is an JSONArray, but should be an JSONObject. Thus, calling other simulatos is canceled.");
	    			break;
	    		}
	    	}
	    	
//	    	JSONObject requests = (JSONObject) return_struct.get("SimRequests");
//	    	if (requests.isEmpty()){continue;}; // break this iteration if there are no requests
	    	   	
	    	// ---- Prepare for Async Requests  (create mosaik proxy)  --------------	    	
	    	MosaikProxy mosaik = this.getMosaik();
	    	Set<String> requestingModels = requests.keySet();

	    	// ---- Request current progress -----------------------------------------	    	
	    	float progress = mosaik.getProgress();
	    	System.out.println("Progress: " + progress);
	    	
	    	// ---- Execute Async Requests -------------------------------------------	    	
	    	//iterate through requesting models
	    	for (String modelname : requestingModels){
	    		Object reqModel = requests.get(modelname);
	    		Map<String,Object> model = null;
	    		if(reqModel instanceof org.json.simple.JSONObject) {
	    			model = (JSONObject) requests.get(modelname);
	    		} else {
	    			JSONArray modelArray = (JSONArray) requests.get(modelname);
	    			if(modelArray.isEmpty()) {
	    				logger.info("Model in SimRequests is not an JSONObject, but an empty JSONArray.");
	    			} else {
	    				logger.error("Model in SimRequests is not an JSONObject. Thus, call is canceled.");
	    			}
	    			break;
	    		}
				inputsCopy = (JSONObject) inputs;
				if(!inputsCopy.isEmpty()) {
					thisModelInputs.putAll((JSONObject) inputsCopy.get(modelname));
				}
				List<Map<String, Object>> asyncAnswers = new JSONArray();

				
				// ---- get Requests -----------------------------------------------------
	    		if (model.containsKey("getRequests")){
	    			Map<String,List<String>> getRequests = (JSONObject)model.get("getRequests");
	    			logger.debug("Found this getRequest: " + getRequests);
	    			
	    			//iterate through addressed simulators
	    			for(String addressed_Entity_ID : getRequests.keySet()){ 
    					List<String> addressed_Parameters = getRequests.get(addressed_Entity_ID);
    					logger.debug("The Request asks this Model: "+addressed_Entity_ID+"\n" + "\t for these data "+ addressed_Parameters+"\n");
    					
    					Map<String,List<String>> this_request = new HashMap<String,List<String>>();
    					this_request.put(addressed_Entity_ID,addressed_Parameters);    					
    					logger.debug("requested: "+this_request);
    					
    					Map<String, Object> data = new HashMap<String,Object>();
    					try {
    						//
    						data = mosaik.getData(this_request); // <------ Actual request to mosaik
    						foundrequest = true;
    						logger.debug("And received: " + data);
    						logger.debug("Old Inputs was: " + inputs);
    						asyncAnswers.add(data);
    						logger.debug("ThisModelInputs: "+thisModelInputs);
    					} catch (NullPointerException BadReturnFromMosaik) {
    						logger.warn("mosaik.getData returned null! ");
    					} finally {}
	    			}
	    			if(foundrequest){	    				
		    			// Copy the Answers to the input Map
		    			thisModelInputs.put("AsyncAnswers",asyncAnswers);
						inputsCopy.put(modelname, thisModelInputs);
						inputs = inputsCopy;
	    			}

	    		}

	    		// ---- set Requests ------------------------------------------------------
	    		// request correct structure (from readthedocs):
	    		// {'src_full_id': {'dest_full_id': {'attr1': 'val1', 'attr2': 'val2'}}}	    		
	    		if (model.containsKey("setRequests")){
	    			Map<String,Object> setRequests = (JSONObject)model.get("setRequests");
	    			logger.debug("Found this setRequest: "+setRequests);									
//					boolean request_executed = false;    						
		    		try {
	    				mosaik.setData(setRequests); // <------ Actual request to mosaik
//	    				request_executed = true;    						
   					} catch (NullPointerException BadReturnFromMosaik) {
   						logger.warn("mosaik.setData returned null! ");
   					} finally {}
	    		}
	    		//assert (getRequests != null || setRequests != null); //This point should not be reached if there are no requests.
	    	}
	    	iteration ++;
    	}
    	Instant end = Instant.now();
    	Duration duration = Duration.between(start, end);
    	logger.info("Did step simulator " + this.simName + " (simulation-time: " + time + ") in " + duration.toHours() + ":" + duration.toMinutes() % 60 + " hours");
    	new_time = recieved_time;
    	return new_time; 
    }

    /**
     * Return the data for the requested attributes in *outputs*
     *
     * @param requests is a mapping of entity IDs to lists of attribute names.
     * @return a mapping of the same entity IDs to maps with attributes and
     *         their values (see {@link
     *         https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html#get-data}).
     * @throws MatlabConnectionException, MatlabInvocationException
     */
    @SuppressWarnings("unchecked")
	public  Map<String, Object> getData(
            Map<String, List<String>> requests) throws MatlabConnectionException, MatlabInvocationException {
    	logger.debug("Java getData called.");
    	//Call Matlab function
    	Object[] recieved = this.matlab.returningEval("MATS"+this.simName + ".getData('"+requests+"')",1);
    	
    	//Object[]->String->JSONValue->JSONObject
    	JSONObject answer = (JSONObject)JSONValue.parse((String)recieved[0]);
    	
    	logger.debug("Java getData executed: " + answer);
    	return answer;	
    }
    /**
     * This method is executed just before the sim process stops.
     *
     * Disconnects from MATLAB
     *
     * @throws MatlabConnectionException, MatlabInvocationException
     */
    @Override
    public void cleanup() throws MatlabConnectionException, MatlabInvocationException {
    	//this.matlab.eval(this.simName+".delete()"); //Destructor call
    	//this.matlab.eval("clear "+this.simName); // Delete handle
		this.matlab.eval("clear");
    	this.matlab.disconnect();
    	logger.info("Disconnected from MATLAB. \n");
        return;
    }

		
} 
