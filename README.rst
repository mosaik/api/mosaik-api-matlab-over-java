=================
mosaik-matlab API
=================

The mosaik-matlab API is an application interface (API) for MATLAB-based simulator
models into the python-based mosaik environment. An approach that uses the existing interface from mosaik to Java was chosen to utilize the 
MATLAB/Java API MatConsoleCtl in order communicate with MATLAB. The realization of the mosaik-matlab API translates 
directly from the Java-API and can be utilized in a nearly identical fashion. Furthermore, all program-components aside from MATLAB 
itself are freely available.
	
Components
==========

An overview of the components involved and links to their detailed documentation can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Components.rst>`_.

Installation
============
The installation instructions can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Installation.rst>`_.

Classes
=======

A description of the newly created classes that make up the API can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Classes.rst>`_.

Implementation & Examples
======================================

A step-by-step implementation guide can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Step-by-Step.rst>`_ and examples can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Examples.rst>`_.


Errors & Debugging
==================

Few common errors have been documented `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Errors.rst>`_ and an example testing script to aid debugging can be found `here <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/rstDocumentation/source/Debugging.rst>`_.
