%% model
% Abstract MATLAB class for a mosaik simulator model 
% used with the MATLAB-API over Java.

classdef (Abstract) model < handle
    %% Standart properties and constructor:
    % Every Simulator model should have the properties <started>,                                                                                                                                                                                                                                                                                                                                                                                          <Eid> and
    % <type>. They should set within the constructor. 
    % (Consider using 'thisModel@model(Eid,params);' for convenience in your constructor.)
    properties
        started
        Eid
        type
        Simulator
        call_me_again
    end
    methods
        function thisModel = model(Eid, params, SimID)
            thisModel.Eid = Eid;
            thisModel.started = true;
            thisModel.Simulator = SimID;
            thisModel.call_me_again = false;
        end
       
        
        function [asyncs,thisModel] = step(thisModel, inputs, step_size, iteration)
        %% asyncs = step(thisModel,time, step_size, inputs)
        % This function is the core of the simulator model. Override this
        % function to describe the behaviour of each model during the
        % simulation steps.
        % The funtion returns the asynchronus requests created during the
        % model step. If you need to perform an asynchronus getData request
        % and need the results in the same step, you can use
        % <call_me_again> and <iteration>. <iteration> is initially set to
        % 1. If you set <thisModel.call_me_again> to true, the step 
        % function will be called again with iteration=iteration+1 from 
        % java, after the asynchronus requests have been performed in
        % Java. I suggest using a syntax of:
        %
        % switch iteration
        %   case 1, [...]
        %       thisModel.call_me_again = true;
        %       asyncs = thisModel.createAsyncRequest(asyncs,'get',<Target Model>,<RequestedAttribute>);
        %   case 2, [...]
        %       %[entityname_absender, val_in, ~] = unravel_inputs(thisModel, inputs, 'val_in');
        %       thisModel.call_me_again = false;  
        %   otherwise
        %       warning('Invalid Iteration attempt. Is something wrong?')
        %       thisModel.call_me_again = false;  
        % end
         asyncs = containers.Map();
            

        end

    end
    methods (Access = protected)
        
        function [entitynames_absender, input_values] = unravel_inputs(thisModel, inputs, input_parameter)
        %%Unravel Inputs - [entityname_absender, input_value] = unravel_inputs(thisModel, inputs, input_parameter)
        % this function can be used to get the values for a single input
        % parameter from the <inputs> Map. <entityname_absender> is a cell
        % array with the entity-IDs of all connected entities that sent a
        % value for this input parameter. 
        % If the input parameter was not found, <input_values> returns as NaN.
        % If exactly one input was found, <input_values> will be that value. 
        % If several were found, <input_values> will be a cell array
        % containing those values.
            entitynames_absender = {};
            input_values = {NaN};
            
            if isempty(inputs) %return if inputs are empty
                warning('input empty!')
                return
            end
            
            if isstruct(inputs) 
                fieldNames = fieldnames(inputs)
                num = numel(fieldNames)
                if num < 1
                    warning('input is empty struct')
                    return
                end
            end   
            
            if inputs.isKey(input_parameter)
                inputs_for_parameter = inputs(input_parameter); %All inputs for this parameter
                if iscell(inputs_for_parameter)
                    inputs_for_parameter = inputs_for_parameter{1};
                end
                entitynames_absender = inputs_for_parameter.keys; %Origins of these inputs

                for i = 1:length(inputs_for_parameter)
                    input_values{i} = inputs_for_parameter(entitynames_absender{i}); %vector of all input values
                end
            else    
                warning('Input parameter %s not found.', input_parameter)
                input_values = NaN;
                return
            end

            if numel(input_values)==1
                input_values = input_values{1};
            end
        end
            
        function asyncs = createAsyncRequest(thisModel,asyncs, type, fullTargetID, targetProperty, varargin)
        %% Create Request: asyncs =  createAsyncRequest(asyncs, type, fullTargetID, targetProperty, targetValue)
        % Areates an asynchronous request to mosaik that will be answered between this and the
        % next iteration of the step() function.
        %
        % <asyncs> is the list of map request (can contain map containers
        % 'getRequests' und 'getRequests').
        % <type> is a string, either 'get' or 'set'.
        % <fullTargetID> is the full ID (as a string) of the model the request addresses.
        % It usually takes the form: Sim_ID.Model_ID
        % <targetProperty> is the Property (string) to be requested or modified
        % <targetValue>, only for <type> == 'set', the new value for the target
        % Property.
            switch type
                case 'get'
                    asyncs = createGetRequest(asyncs, fullTargetID, targetProperty);
                case 'set'
                    if(~numel(varargin)==1), error('Was targetValue passed correctly?'), end
                    targetValue = varargin{1};
                    asyncs = createSetRequest(asyncs, fullTargetID, targetProperty, targetValue);
                otherwise
                    error('Invalid type for Async Request!')
            end


        function asyncs = createGetRequest(asyncs, fullTargetID, targetProperty)
            newRequest = containers.Map({fullTargetID}, {{targetProperty}});

            if asyncs.isKey('getRequests')
                oldRequests = asyncs('getRequests');
                if oldRequests.isKey(fullTargetID)
                    newRequest = containers.Map({fullTargetID},{[oldRequests(fullTargetID) newRequest(fullTargetID)]});
                end
                asyncs('getRequests') = [oldRequests;newRequest];
            else
                getRequests = containers.Map({'getRequests'},{newRequest});
                asyncs = [asyncs; getRequests];
            end

        end
        
        function asyncs = createSetRequest(asyncs, fullTargetID, targetProperty, targetValue)
            
            %initialize
            thisModelFullID = [thisModel.Simulator '.' thisModel.Eid];
            assignmentsToTarget = containers.Map();
            requestsFromThisModel = containers.Map();
            oldSetDataRequests = containers.Map();
            %load existing
            if asyncs.isKey('setRequests')
                oldSetDataRequests = asyncs('setRequests');
                if oldSetDataRequests.isKey(thisModelFullID)
                    requestsFromThisModel = oldSetDataRequests(thisModelFullID);
                    if requestsFromThisModel.isKey(fullTargetID)
                        assignmentsToTarget = requestsFromThisModel(fullTargetID);
                    end                    
                end
            else
                oldSetDataRequests = containers.Map();
            end
            
            %define new assignment
            assignment = containers.Map({targetProperty},{targetValue});
            

            %merge with old asyncs
            assignmentsToTarget = [assignmentsToTarget;assignment];
            requestsFromThisModel = [requestsFromThisModel;containers.Map(fullTargetID,assignmentsToTarget)];
            oldSetDataRequests = [oldSetDataRequests;containers.Map(thisModelFullID, requestsFromThisModel)];

            %setRequests = [setRequests;containers.Map(thisModelFullID, requestsFromThisModel);]
            asyncs = [asyncs;containers.Map('setRequests',oldSetDataRequests)];
            %disp(savejson(asyncs))

        end

        end

    end
end     