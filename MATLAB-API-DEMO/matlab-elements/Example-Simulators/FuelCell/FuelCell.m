classdef FuelCell < model
    %FuelCell 
    %   Simplified fuelcell model
    properties
        offered_power %Current offered_power
        conn_entity
        tank
        tank_size
        max_power
        produce_or_consume
        efficiency
        SoC
        accepted_power
    end
    
    methods
        function FC = FuelCell(Eid, params, SimID)
            FC@model(Eid, params, SimID);
            FC.tank = params.tank_initial;
            FC.tank_size = params.tank_size;
            FC.max_power = params.max_power;
            FC.type = 'FuelCell';
            FC.offered_power = 0;
            FC.produce_or_consume = 'consume';
            FC.SoC = FC.tank/FC.tank_size;
            FC.accepted_power = 0;
            
            %disp(FC)
        end
        
        function asyncs = step(FC, inputs, step_size, ~)
            %keyboard
            %% asyncs = step(inputs, step_size, iteration)
            % The fuelcell is connected to the Battery so that its produced
            % offered_power goes to the battery via:
            % world.connect(fuelcells[0],batteries[0], ('offered_power', 'P_set'), async_requests=True)
            % For demonstration, it requests the current State of Charge
            % from the battery. If the battery is more than 80% full, it
            % will start electrolysis and refill its tank, discharging the
            % battery
            
            %disp(FC)

            asyncs = containers.Map();
            
            %% Read inputs
            [~, accepted_power] = FC.unravel_inputs(inputs, 'accepted_power');

            if iscell(accepted_power) && (numel(accepted_power) == 1)
                if isnan(accepted_power{1})
                    warning('was not able to get attribute accepted_power from inputs.');
                end
            else   
                FC.accepted_power = accepted_power; %Save if input is valid
            end
            
            [~, produce_or_consume] = FC.unravel_inputs(inputs, 'produce_or_consume');
            
            if isstr(produce_or_consume)
                FC.produce_or_consume = produce_or_consume;
            end
            
            %% Perform step()
            % Adjust tank level depending on how much of the "offered"
            % offered_power was accepted last turn
            FC.tank = FC.tank - step_size * FC.accepted_power;  
            
            if strcmp(FC.produce_or_consume,'produce')
                max_i_can_deliver = FC.tank/step_size;
                FC.offered_power = min(FC.max_power, max_i_can_deliver);
            elseif strcmp(FC.produce_or_consume,'consume')
                max_i_can_consume = (FC.tank_size-FC.tank)/step_size;
                FC.offered_power = -min(FC.max_power, max_i_can_consume);
            end
          
            FC.SoC = FC.tank/FC.tank_size; %update State of Charge 
            %fprintf('FC SoC: %d \n',FC.SoC)
        end
    end
end

