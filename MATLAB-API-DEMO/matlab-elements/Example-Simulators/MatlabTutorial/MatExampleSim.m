classdef MatExampleSim < Simulator
    %MatExampleSim: This is a Simulator for a Matlab Example Model
    %functionality   
    properties
    end  

    methods
            %Concrete class Constructor
            function ExSim = MatExampleSim(simName)
            	ExSim@Simulator(simName);
            	%obj@Simulator(simName);
            
            	Ex_meta = struct(); %Define Metadata for model
            	Ex_meta.public = true;
            	Ex_meta.params = {'init_val','val'};
            	Ex_meta.attrs = {'val','delta','type','Eid','eid_prefix'};
            	Ex_meta.any_inputs = true;
            
            	ExSim.metastruct.models = struct('MatExampleModel',Ex_meta);
                  
            end    
    end
end