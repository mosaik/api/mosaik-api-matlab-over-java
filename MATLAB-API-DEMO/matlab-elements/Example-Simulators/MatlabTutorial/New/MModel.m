classdef MModel < model
    % Simple example model for the MATLAB Tutorial 
    properties
        val
        delta
    end
    
    methods
        
        function obj = MModel(Eid, params, SimID)
            
            obj@model(Eid, params, SimID)
            
            if isfield(params,'init_val')
                obj.val = params.init_val;
            else
                obj.val = 0;
            end
            
            if isfield(params,'delta')
                obj.delta = delta;
            else
                obj.delta =1;
            end
            obj.type = 'MModel';
        end
        
        function asyncs = step(obj, inputs, step_size, time, ~)
            
            asyncs=containers.Map();
            
            try
               [~, tmpDelta]= obj.unravel_inputs(inputs,'delta'); %check inputs for errors
            catch
               warning(['No Delta-Input received by' obj.Eid])
               return
            end

            if iscell(tmpDelta) && (numel(tmpDelta)==1)
                if isnan(tmpDelta{1})
                    warning('Was not able to get attribute delta from inputs.');
                end
            else
                obj.delta=tmpDelta;
            end 

            %% perform step 
                obj.val = obj.val + obj.delta;
        
        end
    end
end
