classdef MExampleSim < Simulator
    %MatExampleSim: This is a Simulator for a Matlab Example Model
    %functionality   
    properties
        simulator
        eid_prefix
        %entities
    end  

    methods
            %Concrete class Constructor
            function ExSim = MExampleSim(simName)
            	ExSim@Simulator(simName);
            	%obj@Simulator(simName);
            
            	Ex_meta = struct(); %Define Metadata for model
            	Ex_meta.public = true;
            	Ex_meta.params = {'init_val'};
            	Ex_meta.attrs = {'val','delta'};
            	Ex_meta.any_inputs = true;
            
            	ExSim.metastruct.models = struct('MModel',Ex_meta);
                
                ExSim.simulator = MSimulator();
                ExSim.eid_prefix = 'Model_';
                ExSim.step_size = 60;
                  
            end
            
            function metadata = init(obj, json_sim_params, sid)
                disp('Matlabs Simulator.init method called.')
    
                %% Standard
                obj.initiated = true;         
                sim_params = loadjson(json_sim_params);

                obj.SimID = sid;
            
                if isfield(sim_params,'eid_prefix')
                    obj.eid_prefix = sim_params.eid_prefix;
                end
                %%        
                metadata = savejson('', obj.metastruct); %convert metastruct to json metadata
            end
            

            
            function created = create(obj,number, model_type, json_model_params)
                disp('Matlabs Simulator.create method called.')
                global DEBUG
                if DEBUG
                    save('./DEBUG/create_workspace')
                end
                
                model_params = loadjson(json_model_params);
                
                %Perhaps just necessary for calls from Matlab:
                % Check if model_params contain a cell array.
                % Converting that cell array to struct array, because this is
                % probably a conversion error from loadjson.
                fnames = fieldnames(model_params);
                for i = 1: length(fnames)
                    if iscell(model_params.(fnames{i}))
                        %keyboard
                        disp('Cell entry found in model params during model creation. Converting to struct.')
                        model_params.(fnames{i}) = myCell2Struct(model_params.(fnames{i}));
                    end
                end
                
                
                function [ OutputStruct ] = myCell2Struct( InputCells )
                    %Cell to Struct cenversion: [ OutputStruct ] = myCell2Struct( InputCells )
                    %   Converts a 1xN Cell Array containing Struct with equal fields
                    %   to a 1xN Struct array containing the respective fields.
                    %   Realized as a nested function, because it is only needed at
                    %   this point.
                    
                    if ~ min(size(InputCells))==1
                        error('At leasrt one dimension of the input cell array must be 1.')
                    end
                    
                    for j = 1:length(InputCells)
                        if ~isstruct(InputCells{j})
                            error('CellArray contains non-struct-element')
                        end
                        
                        if exist('OutputStruct','var')
                            OutputStruct = [OutputStruct,InputCells{j}];
                        else
                            OutputStruct = InputCells{j};
                        end
                    end
                end
                
                %% Standard: Creating entities
                Entities = struct();
                created_list = cell(number,1);
                
                % Check if the simulator already has models.
                no_old_fields = 0;
                if ~isempty(fieldnames(obj.entities))
                    % If so, how many of the requested kind?
                    old_fieldnames = fieldnames(obj.entities);
                    for k = 1:numel(old_fieldnames)
                        no_old_fields = no_old_fields + strcmp(obj.entities.(old_fieldnames{k}).type,model_type);
                    end
                end
                
                % Add their number to all new entitiy counters.
                for entity_counter = 1+no_old_fields:number+no_old_fields
                        % Generate entity ID
                        Eid = sprintf('%s_%s',model_type,num2str(entity_counter));
                        % Create entity (default)
                        evalString = sprintf('obj.simulator.add_model(''%s'',model_params,obj.SimID) ',Eid);
                        Entity = eval(evalString);
                        % Append created entity to struct
                        obj.entities.(Entity.models(entity_counter).Eid) = Entity.models(entity_counter);
                        
                        %Create nested list of created objects
                        created_list{entity_counter - no_old_fields} = struct;
                        created_list{entity_counter - no_old_fields}.eid =  Entity.models(entity_counter).Eid;
                        created_list{entity_counter - no_old_fields}.type = Entity.models(entity_counter).type;
                        created_list{entity_counter - no_old_fields}.rel = {};
                        created_list{entity_counter - no_old_fields}.children = {};
                end
                
                created_struct = myCell2Struct(created_list);
                
                %%
                created = savejson('', created_struct);
                
                if numel(created_list) == 1 %If only entity was created, savejson does not add the []-brackets indicating the list type for java.
                    created = ['[   ',created,'    ]'];
                end
                
                %created = created(2:end-2) %savejson liefert ein set von klammern zu viel, was in python fuer fehler sorgt.
            end
                      
    end
    
end