classdef BatteryCell < handle
    % basic model of a battery cell
    properties (SetAccess = private)
        % dynamic attributes
        E;  % el. energy stored in cell [J]
        % static attributes
        E_max;  % max cell capacity [J]
        P_max;  % max cell power, positive value, max. charging power [W]
        P_min;  % min cell power, negative value, max. discharging power[W])
    end
    
    methods
        function batterycell = BatteryCell(cell_params) % constructor
            batterycell.E = cell_params.E;
            batterycell.E_max = cell_params.E_max;
            batterycell.P_min = cell_params.P_min;
            batterycell.P_max = cell_params.P_max;
            % set parameters
        end
        
        function P_get = step(batterycell, P_set, step_size)
            p=[-2.10956640161585, 0.430355559269236, 97.1107699164692]; %Polynom-Fit of battery efficiency
%% !! artificially setting efficiency to 80% !!            
min_efficiency = 0.8;
%% !!
            if P_set < 0  % Discharge
                P_set_P_limited = max(batterycell.P_min, P_set); %Limited to Battery Power Specs
                P_set_norm=P_set_P_limited/(batterycell.E_max/3600); %Normalize Power to Cell Size
                efficiency=(p(1)*P_set_norm^2+p(2)*P_set_norm^1+p(3))/100; %Calculate Efficiency
                efficiency = max([efficiency,min_efficiency]);
                
                E_step_Batt=P_set_P_limited /efficiency* step_size; %Calculate E_Step (Disch Case) for the Battery  with Efficiency
                E_remaining = batterycell.E + E_step_Batt ; %Discharge with Eff
                if E_remaining >= 0 %Will not be fully discharged
                     batterycell.E = batterycell.E +E_step_Batt;
                     P_get = P_set_P_limited;
                else %Will be fully discharged
                   P_avg=-batterycell.E/step_size; %Avg Power  at complete disch
                   P_avg_norm=P_avg/(batterycell.E_max/3600); %Normalize Avg Power to Cell Size
                   efficiency=(p(1)*P_avg_norm^2+p(2)*P_avg_norm^1+p(3))/100; %Calculate Efficiency                
                   batterycell.E = 0; %Cell will be empty
                   P_get = P_avg*efficiency;
                end
            else %Charge
                P_set_P_limited = min(batterycell.P_max, P_set); %Limited to Battery Power Specs
                P_set_norm=P_set_P_limited/(batterycell.E_max/3600); %Normalize Power to Cell Size
                efficiency=(p(1)*P_set_norm^2+p(2)*P_set_norm^1+p(3))/100; %Calculate Efficiency
                efficiency = max([efficiency,min_efficiency]);
                
                E_step_Batt=P_set_P_limited*efficiency* step_size; %Calculate E_Step (Ch Case) for the Battery  with Efficiency
                E_after = batterycell.E + E_step_Batt ; %Charge with Eff
                if E_after > batterycell.E_max %Will be overcharged
                     P_avg=(batterycell.E_max-batterycell.E)/step_size; %Avg Cell Power  at complete ch
                     P_avg_norm=P_avg/(batterycell.E_max/3600); %Normalize Avg Power to Cell Size
                     efficiency=(p(1)*P_avg_norm^2+p(2)*P_avg_norm^1+p(3))/100; %Calculate Efficiency   
                     batterycell.E = batterycell.E_max; %Cell will be full
                     P_get = P_avg/efficiency;
                else %Will NOT be overcharged
                     batterycell.E = batterycell.E +E_step_Batt;
                     P_get = P_set_P_limited;
                end
            end

        end
        
            
            function cell_state = get_state(batterycell)
                % translates batterycell object to struct
                % BatteryCell(batterycell.get_state()) delivers copy of
                % batterycell
                cell_state = struct('E', batterycell.E, ...
                    'E_max', batterycell.E_max,...
                    'P_min', batterycell.P_min, ...
                    'P_max', batterycell.P_max...
                    );
            end
        end
    end
    
