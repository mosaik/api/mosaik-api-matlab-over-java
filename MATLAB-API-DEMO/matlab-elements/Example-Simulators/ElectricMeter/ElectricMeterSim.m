classdef ElectricMeterSim < Simulator
    % Electricity meter simulator 
    % Matlab simulator for the "Electricmeter"-model.
    
    properties
        
    end
    
    methods
        %Concrete class Constructor
        function ems = ElectricMeterSim(simName)
            ems@Simulator(simName);

            meter_meta = struct(); %Define Metadata for counter-model
            meter_meta.public = true;
            meter_meta.params = {'init_val','empty'};
            meter_meta.attrs = {'started','conn_entity','Power','type','Eid','Energy'};
            meter_meta.any_inputs = true;
            
            ems.metastruct.models = struct('ElectricMeter',meter_meta);
                  
        end

    end
    
end
