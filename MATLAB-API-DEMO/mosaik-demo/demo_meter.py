__author__ = 'rengel'

import itertools
import random
import time
import json
from subprocess import call
from mosaik.util import connect_randomly, connect_many_to_one
import mosaik
import os

sim_config = {
    'ElectricMeterSim': {
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s ElectricMeterSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'cmd': 'mosaik-hdf5 %(addr)s',
    },
    'HouseholdSim': {
        'python': 'householdsim.mosaik:HouseholdSim',
        # 'cmd': 'mosaik-householdsim %(addr)s',
    },
    'PyPower': {
        'python': 'mosaik_pypower.mosaik:PyPower',
        # 'cmd': 'mosaik-pypower %(addr)s',
    },
    'WebVis': {
        'cmd': 'mosaik-web -s 0.0.0.0:8000 %(addr)s',
    },
}

START = '2014-01-01 00:00:00'
# END = 1 * 3600 * 5  # simulation time in seconds
END = 3*60
PV_DATA = 'data/pv_10kw.csv'
PROFILE_FILE = 'data/profiles.data.gz'
GRID_NAME = 'demo_lv_grid'
GRID_FILE = 'data/%s.json' % GRID_NAME


def main():
    random.seed(23)
    world = mosaik.World(sim_config)
    create_scenario(world)
    world.run(until=END)  # As fast as possilbe
    # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec


def create_scenario(world):
    # Start simulators
    pypower = world.start('PyPower', step_size=15 * 60)
    hhsim = world.start('HouseholdSim')
    pvsim = world.start('CSV', sim_start=START, datafile=PV_DATA)
    emSim = world.start('ElectricMeterSim', step_size=60, parallelize=False, numCores=0)

    # Instantiate models
    grid = pypower.Grid(gridfile=GRID_FILE).children
    houses = hhsim.ResidentialLoads(sim_start=START, profile_file=PROFILE_FILE, grid_name=GRID_NAME).children
    pvs = pvsim.PV.create(20)
    em = emSim.ElectricMeter.create(houses.__len__(), init_val=0)

    # Connect entities
    connect_buildings_to_grid(world, houses, grid)
    connect_randomly(world, pvs, [e for e in grid if 'node' in e.eid], 'P')
    connect_meters_to_houses(world, houses, em)

    # Database
    db = world.start('DB', step_size=60, duration=END)
    hdf5 = db.Database(filename='demo_meter.hdf5')
    connect_many_to_one(world, houses, hdf5, 'P_out')
    connect_many_to_one(world, pvs, hdf5, 'P')
    connect_many_to_one(world, em, hdf5, 'Energy')

    nodes = [e for e in grid if e.type in ('RefBus, PQBus')]
    connect_many_to_one(world, nodes, hdf5, 'P', 'Q', 'Vl', 'Vm', 'Va')

    branches = [e for e in grid if e.type in ('Transformer', 'Branch')]
    connect_many_to_one(world, branches, hdf5, 'P_from', 'Q_from', 'P_to', 'P_from')

    # Web visualization
    webvis = world.start('WebVis', start_date=START, step_size=60)
    webvis.set_config(ignore_types=['Topology', 'ResidentialLoads', 'Grid',
                                    'Database'])
    vis_topo = webvis.Topology()

    connect_many_to_one(world, nodes, vis_topo, 'P', 'Vm')
    webvis.set_etypes({
        'RefBus': {
            'cls': 'refbus',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 30000,
        },
    })
    connect_many_to_one(world, houses, vis_topo, 'P_out')
    webvis.set_etypes({
        'House': {
            'cls': 'load',
            'attr': 'P_out',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 3000,
        },
    })
    connect_many_to_one(world, pvs, vis_topo, 'P')
    webvis.set_etypes({
        'PV': {
            'cls': 'gen',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': -10000,
            'max': 0,
        },
    })
    connect_many_to_one(world, em, vis_topo, 'Energy')
    webvis.set_etypes({
        'ElectricMeter': {
            'cls': 'load',
            'attr': 'Energy',
            'unit': 'E [J]',
            'default': 0,
            'min': 0,
            'max': 100000000,
        },
    })


def connect_buildings_to_grid(world, houses, grid):
    buses = filter(lambda e: e.type == 'PQBus', grid)
    buses = {b.eid.split('-')[1]: b for b in buses}
    house_data = world.get_data(houses, 'node_id')

    for house in houses:
        node_id = house_data[house]['node_id']
        world.connect(house, buses[node_id], ('P_out', 'P'))


def connect_meters_to_houses(world, houses, meters):
    for n in range(0, meters.__len__()):
        world.connect(houses[n], meters[n], ('P_out', 'Power'))


if __name__ == '__main__':

    no_mat_sessions = 1  # number of MATLAB sessions needed (= number of matlab simulators)
    debug_matlab = True  # open matlab hidden (for quick running) or openly (for debugging)
    cwd = sim_config.get('ElectricMeterSim').get('cwd')  # matlab folder path
    if (debug_matlab):
        debug_param = "debug"
    else:
        debug_param = "nodebug"

    # calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
    os.system('java -cp StartMatlab.jar de.offis.mosaik.api.matlab.StartMatlab ' + cwd + ' ' + str(
        no_mat_sessions) + ' ' + debug_param)

    print('Waiting for MATLAB to get ready')
    time.sleep(10)

    main()
