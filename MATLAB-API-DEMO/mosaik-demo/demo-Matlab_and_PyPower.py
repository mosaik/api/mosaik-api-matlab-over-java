import itertools
import random
import time
import json
from subprocess import call
from mosaik.util import connect_randomly, connect_many_to_one
import mosaik
import os


sim_config = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'DB': {
        'cmd': 'mosaik-hdf5 %(addr)s',
    },
    'HouseholdSim': {
        'python': 'householdsim.mosaik:HouseholdSim',
        # 'cmd': 'mosaik-householdsim %(addr)s',
    },
    'PyPower': {
        'python': 'mosaik_pypower.mosaik:PyPower',
        # 'cmd': 'mosaik-pypower %(addr)s',
    },
    'WebVis': {
        'cmd': 'mosaik-web -s 0.0.0.0:8000 %(addr)s',
    },
    'MAT-FCSimulator': {
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s FCSimulator',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'MAT-MatBatSim': {
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s MatBatSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'MAT-ChargeControlSim': {
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s ChargeControlSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    }
}

START = '2014-01-01 00:00:00'
END = 1 * 3600*5 # simulation time in seconds
PV_DATA = 'data/pv_10kw.csv'
PROFILE_FILE = 'data/profiles.data.gz'
GRID_NAME = 'demo_lv_grid'
GRID_FILE = 'data/%s.json' % GRID_NAME

with open("Battery_Params.json") as json_file:
    Battery_Params = json.load(json_file)
    json_file.closed

def main():
    random.seed(23)
    world = mosaik.World(sim_config)
    create_scenario(world)
    world.run(until=END)  # As fast as possilbe
    # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec


def create_scenario(world):
    # Start simulators
    pypower = world.start('PyPower', step_size=15*60)
    hhsim = world.start('HouseholdSim')
    pvsim = world.start('CSV', sim_start=START, datafile=PV_DATA)

    MatBatSim = world.start('MAT-MatBatSim', step_size=60)
    FCSim = world.start('MAT-FCSimulator', step_size=60)
    CCSim = world.start('MAT-ChargeControlSim', step_size=60)

    # Instantiate models
    grid = pypower.Grid(gridfile=GRID_FILE).children
    houses = hhsim.ResidentialLoads(sim_start=START,
                                    profile_file=PROFILE_FILE,
                                    grid_name=GRID_NAME).children
    pvs = pvsim.PV.create(20)


    batteries = MatBatSim.Battery.create(1, age = Battery_Params['age'],
                                               batterycells = Battery_Params['batterycells'])
    fuelcells = FCSim.FuelCell.create(1, max_power = 15000, tank_size = 1e8, tank_initial = 5e7)
    charge_controls = CCSim.ChargeControl.create(1, may_produce = 1)

    # Connect entities
    connect_buildings_to_grid(world, houses, grid)
    connect_randomly(world, pvs, [e for e in grid if 'node' in e.eid], 'P')



    # Database
    db = world.start('DB', step_size=60, duration=END)
    hdf5 = db.Database(filename='demo.hdf5')
    connect_many_to_one(world, houses, hdf5, 'P_out')
    connect_many_to_one(world, pvs, hdf5, 'P')

    nodes = [e for e in grid if e.type in ('RefBus, PQBus')]
    connect_many_to_one(world, nodes, hdf5, 'P', 'Q', 'Vl', 'Vm', 'Va')

    branches = [e for e in grid if e.type in ('Transformer', 'Branch')]
    connect_many_to_one(world, branches, hdf5,
                        'P_from', 'Q_from', 'P_to', 'P_from')


    # connect additional entities

    world.connect(fuelcells[0],batteries[0], ('offered_power', 'P_set'), async_requests=True)
    world.connect(nodes[37],charge_controls[0], ('P', 'input_power'))
    world.connect(charge_controls[0],batteries[0], ('power_out', 'P_set'), async_requests=True)

    # Web visualization
    webvis = world.start('WebVis', start_date=START, step_size=60)
    webvis.set_config(ignore_types=['Topology', 'ResidentialLoads', 'Grid',
                                    'Database'])
    vis_topo = webvis.Topology()



    connect_many_to_one(world, nodes, vis_topo, 'P', 'Vm')
    webvis.set_etypes({
        'RefBus': {
            'cls': 'refbus',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 30000,
        },
        'PQBus': {
            'cls': 'pqbus',
            'attr': 'Vm',
            'unit': 'U [V]',
            'default': 230,
            'min': 0.99 * 230,
            'max': 1.01 * 230,
        },
    })

    connect_many_to_one(world, houses, vis_topo, 'P_out')
    webvis.set_etypes({
        'House': {
            'cls': 'load',
            'attr': 'P_out',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 3000,
        },
    })

    connect_many_to_one(world, pvs, vis_topo, 'P')
    webvis.set_etypes({
        'PV': {
            'cls': 'gen',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': -10000,
            'max': 0,
        },
    })

    connect_many_to_one(world, fuelcells, vis_topo, 'tank')
    webvis.set_etypes({
        'FuelCell': {
            'cls': 'gen',
            'attr': 'tank',
            'unit': 'E [J]',
            'default': 0,
            'min': 0,
            'max': 1000000000,
        },
    })
    connect_many_to_one(world, batteries, vis_topo, 'E')
    webvis.set_etypes({
        'Battery': {
            'cls': 'load',
            'attr': 'E',
            'unit': 'E [J]',
            'default': 0,
            'min': 0,
            'max': 1000000000,
        },
    })
    connect_many_to_one(world, charge_controls, vis_topo, 'power_out')
    webvis.set_etypes({
        'ChargeControl': {
            'cls': 'gen',
            'attr': 'power_out',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 1,
        },
    })

def connect_buildings_to_grid(world, houses, grid):
    buses = filter(lambda e: e.type == 'PQBus', grid)
    buses = {b.eid.split('-')[1]: b for b in buses}
    house_data = world.get_data(houses, 'node_id')
    for house in houses:
        node_id = house_data[house]['node_id']
        world.connect(house, buses[node_id], ('P_out', 'P'))

if __name__ == '__main__':

    no_mat_sessions = 3 # number of MATLAB sessions needed (= number of matlab simulators)
    debug_matlab = True # open matlab hidden (for quick running) or openly (for debugging)
    cwd = sim_config.get('MAT-FCSimulator').get('cwd')  # matlab folder path
    if(debug_matlab):
        debug_param = "debug"
    else:
        debug_param = "nodebug"

    # calls the Script that calls the java class that preopens MATLAB, then waits for initialization
    os.system('java -cp StartMatlab.jar de.offis.mosaik.api.matlab.StartMatlab ' + cwd + ' ' + str(no_mat_sessions) + ' ' + debug_param)
    print('Waiting for MATLAB to get ready')
    time.sleep(2)

    main()


