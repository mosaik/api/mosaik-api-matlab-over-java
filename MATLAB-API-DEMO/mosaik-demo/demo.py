__author__ = 'rengel'
import itertools
import random
import time
import json
from subprocess import call
from mosaik.util import connect_randomly, connect_many_to_one
import mosaik
import os


sim_config = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'WebVis': {
        'cmd': 'mosaik-web -s 0.0.0.0:8000 %(addr)s',
    },
    'MAT-FCSimulator': {
        #'cmd': 'java -jar MatSim.jar %(addr)s FCSimulator',
        #'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s FCSimulator',
	'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'MAT-MatBatSim': {
        #'cmd': 'java -jar MatSim.jar %(addr)s MatBatSim',
        #'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
        'cmd': 'java -jar MatlabSimulator.jar %(addr)s MatBatSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    },
    'MAT-RPGSim': {
        #'cmd': 'java -jar MatSim.jar %(addr)s RPGSim',
        #'cwd': 'H:\\mosaik-api-matlab-over-java\\MATLAB-API-DEMO\\matlab-elements'
	'cmd': 'java -jar MatlabSimulator.jar %(addr)s RPGSim',
        'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
    }
}

START = '2014-01-01 00:00:00'
END = 1 * 3600*5 # simulation time in seconds


with open("Battery_Params.json") as json_file:
    Battery_Params = json.load(json_file)
    json_file.closed

def main():
    random.seed(23)
    world = mosaik.World(sim_config)
    create_scenario(world)
    world.run(until=END)  # As fast as possilbe
    # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec


def create_scenario(world):
    # Start simulators


    MatBatSim = world.start('MAT-MatBatSim', step_size=60)
    FCSim = world.start('MAT-FCSimulator', step_size=60)
    RPGSim = world.start('MAT-RPGSim', step_size=60)

    # Instantiate models

    batteries = MatBatSim.Battery.create(1, age = Battery_Params['age'],
                                               batterycells = Battery_Params['batterycells'])
    fuelcells = FCSim.FuelCell.create(1, max_power = 15000, tank_size = 1e8, tank_initial = 1e8)
    generators = RPGSim.Generator.create(1, max_power = 10000, may_produce = 1)

    # Connect entities

    world.connect(fuelcells[0],batteries[0], ('offered_power', 'P_set'), async_requests=True)
    world.connect(generators[0],batteries[0], ('power_out', 'P_set'), async_requests=True)

    # Web visualization
    webvis = world.start('WebVis', start_date=START, step_size=60)
    webvis.set_config(ignore_types=['Topology', 'ResidentialLoads', 'Grid',
                                    'Database'])
    vis_topo = webvis.Topology()



    connect_many_to_one(world, fuelcells, vis_topo, 'tank')
    webvis.set_etypes({
        'FuelCell': {
            'cls': 'gen',
            'attr': 'tank',
            'unit': 'E [J]',
            'default': 0,
            'min': 0,
            'max': 1000000000,
        },
    })
    connect_many_to_one(world, batteries, vis_topo, 'E')
    webvis.set_etypes({
        'Battery': {
            'cls': 'load',
            'attr': 'E',
            'unit': 'E [J]',
            'default': 0,
            'min': 0,
            'max': 1000000000,
        },
    })
    connect_many_to_one(world, generators, vis_topo, 'power_out')
    webvis.set_etypes({
        'Generator': {
            'cls': 'gen',
            'attr': 'power_out',
            'unit': '[W]',
            'default': 0,
            'min': 0,
            'max': 1,
        },
    })


if __name__ == '__main__':

    no_mat_sessions = 3 #number of MATLAB sessions needed (= number of matlab simulators)
    debug_matlab = True #open matlab hidden (for quick running) or openly (for debugging)
    cwd = sim_config.get('MAT-FCSimulator').get('cwd')  #matlab folder path
    if(debug_matlab):
        debug_param = "debug"
    else:
        debug_param = "nodebug"

    #calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
    os.system('java -cp StartMatlab.jar de.offis.mosaik.api.matlab.StartMatlab ' + cwd + ' ' + str(no_mat_sessions) + ' ' + debug_param)
    #os.system('sh ' + cwd + '/StartMatlab.sh ' + str(no_mat_sessions) + ' ' + debug_param)
    #print(cwd +"/StartMatlab.sh" + " " + cwd + " " + str(no_mat_sessions) + " " + debug_param)
    #call([cwd + "/StartMatlab.sh",str(no_mat_sessions),debug_param]);
    #call(cwd +"/StartMatlab.sh" + " " + cwd + " " + str(no_mat_sessions) + " " + debug_param, shell = True)
    print('Waiting for MATLAB to get ready')
    time.sleep(10)

    main()


