============
Installation
============

This section guides through the installation procedure.

Prerequisites
=============

This tutorial is based the following software versions. Older and newer versions might also work, but were not tested:

        * **MATLAB**  - 	MATLAB 2019a and 2020a
        * **JSONLab Toolbox**   - The JSONLab Toolbox is used as JSON encoder and decoder. Versions- 1.2 and 2.0
        * **Python**  - 	Python 3.7.7 (64-bit)
        * **Java**    -		Java SE Development Kit 8 Update 251 (64-bit)

The installation procedure of these programs is well documented and can be found on the respective websites. To make any modifications to the applications, IDEs might be installed. Eclipse Java Mars for Java and PyCharm Community Edition were used by the developers.

Installing mosaik
=================
As this is an API for mosaik, mosaik has to be set up before proceeding to the installation of the
mosaik-matlab API. It is recommended to follow the installation guide at https://mosaik.readthedocs.org.

The mosaik-matlab API
=====================
To include the mosaik-matlab API, select a location for your MATLAB-directory.
The path of the MATLAB-directory must be set in the cwd-variable in the main python script in the *sim_config* variable.
For a MATLAB-simulator to work, the .m-files of the simulator and all models must simply be located in this directory or any sub-directory of it.
This directory should contain all of the following files:

        1. Java Archive: MatlabSimulator.jar
        2. cmd-file: StartMatlab.cmd
        3. Java class file: StartMatlab.class
        4. Matlab (abstract) class file: Simulator.m
        5. Matlab (abstract) class file: model.m
        6. Matlab class files: The inheriting simulator and model files of the realized simulator.
           All matlab files can also be moved to sub-directories if so desired.

