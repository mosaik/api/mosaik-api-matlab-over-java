==========
Components
==========

In this section, the existing components are listed and a reference to their respective documentation is provided.
Java, Python and MATLAB are naturally a prerequisite and assumed to be known.

mosaik
======

Mosaik is a python-based co-simulation framework for energy systems used to coordinate simulators in a common context.
It is assumed that the reader has a basic familiarity with mosaik and its working principles.
A full documentation of mosaik can be found in the online documentation (`mosaik.readthedocs.org <https://mosaik.readthedocs.org/en/latest/overview.html>`_).


mosaik Java API
===============
Together with mosaik a java application interface is provided, which represents a high-level API for mosaik. The matlab interface presented here bases on that mosaik Java API.
The Java API provides an abstract base class *Simulator*. It can be used to create an inheriting simulator class that overwrites the methods in Java's *Simulator* class.
In order to use the Java API to communicate with MATLAB, the Java Class *MatlabSimulator* was written inheriting from *Simulator*. When called, this class creates a connection to MATLAB using MatConsoleCtl. It overwrites all the methods provided by Java's *Simulator* class in such a way that all input parameters are passed to a corresponding abstract MATLAB-class, also called *Simulator*.
The abstract *Simulator* class in MATLAB can be used in the same way as the abstract *Simulator* class in Java.
The Java API can be found at `gitlab.com/mosaik/mosaik-api-java <https://gitlab.com/mosaik/mosaik-api-java>`_.



MatConsoleCtl
=============

MatConsoleCtl is a MATLAB/Java API that allows for calling MATLAB from Java.
It works by creating a proxy object in Java. This object has the methods *eval* and *returningEval*, which allow the calling of
non-returning or returning MATLAB functions respectively.
The Java-class *MatlabSimulator* creates such a proxy (proxy name is matlab) in its constructor.
It calls MATLAB-functions later in the manner of::

        MatlabSimulator.matlab.eval("my_matlab_function("+ <Argument_from_Java> +");")


Not all types of variables work for input and output arguments. For remarks on which variable types are valid, see the documentation of the *MatlabSimulator* class below.
A general documentation of MatConsoleCtl can be found at `github.com/diffplug/matconsolectl <https://github.com/diffplug/matconsolectl>`_.


Jsonlab
=======

Jsonlab is a freely available MATLAB toolbox for the handling of data in the json format. The abstract *Simulator* class in MATLAB uses this toolbox to convert the json-inputs from Java into MATLABs struct-format and vice versa.
The base toolbox is available from the `mathworks file exchange <http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-file>`_ and has to be installed to use the mosaik-matlab API.
