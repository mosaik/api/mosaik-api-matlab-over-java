============
Introduction
============

This documentation describes an application programming interface (API) for MATLAB-based simulator
models into the python-based mosaik environment.
An approach that uses the existing interface from mosaik to Java was chosen to utilize the MATLAB/Java API
MatConsoleCtl in order communicate with MATLAB.
The realization of the mosaik-matlab API translates directly from the Java-API and can be utilized
in a nearly identical fashion. Furthermore, all program-components aside from MATLAB itself are freely available.

.. figure:: /MatAPIdocstatic/Dataflow_normal.jpg
    :width: 500
    :align: center
    :alt: Step call to the MATLAB API
    
    Illustration of a single step()-call from mosaik to a MATLAB-based simulator.


In the figure above, the general dataflow within the mosaik-matlab API is illustrated. With this documentation,
a guideline is provided to easily understand the API and its usage.