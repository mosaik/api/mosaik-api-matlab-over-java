=============
Debug Scripts
=============

This mosaik-matlab API uses the detour over java to communicate between mosaik and MATLAB.
For debugging purposes, this means that there are three programming languages and compilers
that could produce an error, which can be a source of confusion.
As a solution to this problem, the Test-scripts in Java and MATLAB were created, which can call
and therefor test a simulator model with the same input parameters that the mosaik environment provides.
The testing scripts from MATLAB are very straightforward and should be created as needed.
The testing scripts from Java however were not only very useful to understand the way of
communication between Java and MATLAB, but were also sometimes tricky with the variable
types. Therefore, the testing script for a very simple simulator with a model for
an electricity-meter has been included: ::

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import org.json.simple.JSONObject;
        import matlabcontrol.MatlabConnectionException;
        import matlabcontrol.MatlabInvocationException;

        public class TestMatlabZaehlerSimulator {
                @SuppressWarnings("unchecked")
                public static void main(String[] args) throws MatlabConnectionException, MatlabInvocationException
                        {
                        // Initiate Matlab 
                        StartMatlab.main(args);
                        MatlabSimulator MySim = new MatlabSimulator("Zaehlersimulator");
        //Testing init function
                        JSONObject simParams = new JSONObject();
                        simParams.put("step_size",60);
                        MySim.init("CountSID", simParams);
                        //System.out.print((JSONObject)MySim.meta);
        //Testing create function
                        JSONObject modelParams = new JSONObject();
                        modelParams.put("init_val",0);
                        MySim.create(2, "Stromzaehler", modelParams);	
                        //MyCSim.create(2, "Counter", modelParams);
        //Testing step function
                        JSONObject inputs = new JSONObject();
                        inputs.put("step_size",60);	
                        JSONObject nodename = new JSONObject();
                        nodename.put("MyNodeName", 64); //This is how it comes from mosaik, 64 being the actual power
                        JSONObject Power_node1 = new JSONObject();
                        Power_node1.put("Power", nodename);
                        inputs.put("Stromzaehler_1", Power_node1);		
                        @SuppressWarnings("unused")
                        long newtime = MySim.step(1, inputs);
                        newtime = MySim.step(1, inputs);
                        newtime = MySim.step(1, inputs);
        //Testing getData function
                        JSONObject requested_eids = new JSONObject();
                        List<String> fields = new ArrayList<String>();
                        fields.add("Energy");
                        fields.add("Eid");
                        //fields.add("''Eid''");
                        //String fields = "\n[\n \"val\",\n \"Eid\"\n]";
                        requested_eids.put("Stromzaehler_1", fields);
                        System.out.print("Reqest:" + requested_eids+"\n");
                        JSONObject Answer = (JSONObject)MySim.getData(requested_eids);	
                        System.out.print("Answer"+Answer+"\n");
        //Cleanup
                        MySim.cleanup();
                }

        }


