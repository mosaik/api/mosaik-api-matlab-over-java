====================
Relevant Class Files
====================
This section introduces the newly created classes that make up the API.
When using this API to implement a simulator in mosaik, only the MATLAB-part should be of importance, 
because the interaction with Java is part of the API and should need no further modifications.

MATLAB
=======

Simulator
---------
This is the abstract simulator class that connects to mosaik. It is called by the Java class *MatlabSimulator* 
over MatConsoleCtl. To implement a simulator, create a non-abstract simulator class inheriting from this class.  
Please feel free to use the irregularly indented debugging outputs (by commenting/uncommenting). 

Simulator class properties:
^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

        properties (SetAccess = protected)
           simName
           SimID
           metastruct = struct('models',struct(),'api_version','2.0');
           initiated = false;
           step size
           entities = struct();

Most of these variables are self-explanatory. 
*simName* specifies the simulator name, *SimID* is the simulator ID as assigned by mosaik, *initiatied* is a boolean specifying if the simulator 
is initiated, the *api_version* is required by mosaik to be set to 2 and the *step_size* is the default step size in mosaik.
Furthermore, the *entities* struct is initialized in the constructor and will later (after the first create-call) hold all associated simulator 
model instances identified by their entity ID (*EID*).
The *metastruct* is equally important.
It should be set in the constructor of a simulator and will be returned to mosaik as the return value of the *init* method.

Simulator class methods:
^^^^^^^^^^^^^^^^^^^^^^^^
::

        methods (SetAccess = public)
           obj = Simulator(simName)
           metadata = init(obj, json_sim_params, sid)
           created = create(obj,number, model_type, json_model_params)
           return_json = step(obj,current_time, json_inputs, iteration)
           Json_Answer = getData(obj,requested)
           delete(obj)

Here, the method functionality of the abstract simulator class is described. All these methods
can, if necessary, be overwritten or modified in any inheriting methods. However for most
simulators, simply setting the correct *metastruct* in the constructor will be the only necessary
changes when inheriting from this class.

        1. The constructor
                  takes *simName* as input string and saves it. For convenience, the *metastruct* is defined and set in the constructor, although it is returned to mosaik from *init*.
        2. metadata = init(obj, json_sim_params, sid)
                   Initializes the Simulator using *json_sim_params* and returns the metadata *metastruct* parameter. The *metastruct* is very important for the later integration into mosaik. This parameter has to contain the information about the initiation of the simulator (via the *metastruct.params*) and which parameters are available to mosaik during runtime (via the *metastruct.attrs*). Mosaik can never access any parameters that are not specified in the *metastruct.attrs* parameter.
        3. created = create(obj,number, model_type, json_model_params)
                   Creates number of instances of the simulator model *model_type* using the model params for all these instances. Returns the created instances as a struct(). The Entity IDs *Eid* are numbered continuously for every model, even if some entities of the requested model already exist. *json_model_params* should be a JSON object containing the initialization parameters for the models.
        4. return_json = step(obj,current_time, json_inputs, iteration)
                   This is the method that calls all connected simulator models in each step. *json_inputs* should be a JSON object mapping all the simulator models (by their *Eid*) to their specific input parameters. Only those models (i.e. their *step* methods) that receive at least one such input will be invoked each step. Optionally, *json_inputs* may also contain a parameter *step_size*. In that case, this specific step will be executed with a difierent step size than that which is specified as a simulator property. *iteration* is only needed if some models make a *getData* request that they want to have the answer to before the next step. iteration starts out at 1. If any model of the simulator set its *model.call_me_again* to true, those models will be called a second time in the same step from java with iteration = 2.
        5. Json_Answer = getData(obj,requested)
                   This method is invoked when another simulator or model makes a *getData* request to this model. *requested* should be a JSON object mapping Model-IDs of requested entities to the names of their requested fields. Returns *Json_Answer* is a simple JSON object just like requested, but mapping values to the requested fields.


model
-----

This is the abstract MATLAB class for mosaik simulator models used with the mosaik-matlab API
over Java. The abstract model sets the generally needed properties for any simulator model.
Furthermore, it provides a method for the simple unraveling of mosaik's JSON-formatted input
parameters and for the simple and standardized construction of asynchronous requests to other
models.

model class properties:
^^^^^^^^^^^^^^^^^^^^^^^
:: 

        properties (SetAccess = public)
        started
        Eid
        type
        Simulator
        call_me_again

Every Simulator model should have the properties *started* (boolean), *Eid* (ID as valid for
mosaik), *type* (as specified in the parent simulator's *metastruct*) and *call_me_again* (boolean
for multiple calls in one step(), which is only necessary in special situations). They should be set within
the constructor. (Consider using ``obj@model(Eid,params);`` for convenience in the inheriting
class' constructor.)

model class methods:
^^^^^^^^^^^^^^^^^^^^
::

        methods (SetAccess = public)
           asyncs = step(thisModel, inputs, step_size, iteration)

        methods (SetAccess = protected)
           [entitynames_absender, input_values] = ...
                unravel_inputs(thisModel, inputs, input_parameter)
           asyncs = createAsyncRequest(thisModel,asyncs, type,...
                 fullTargetID, targetProperty, varargin)

.

        1. asyncs = step(thisModel, inputs, step_size, iteration)
                   This function is the core of the simulator model. Override this function to describe the behavior of each model during the simulation steps. If your model has any asynchronous requests to submit, construct them using the *createAsyncRequest* method and return them in the *asyncs* parameter. If no asynchronous requests are desired, leave it empty with *asyncs = containers.Map();* Normally, the asynchronous requests will be executed after the step of this simulator returns. If you are submitting a *getData* request for which you need the answer within the same step, you need to make use of the *iteration* parameter as described in "Special case" of the "Step-by-Step Implementation Guide" section of this documentation
        2. [entitynames absender, input values] = unravel inputs(thisModel, inputs, input_parameter)
                   This function can be used to get the values for a single *input_parameter* from the *inputs* map. *entityname_absender* is a cell array with the entity IDs of all connected entities that sent a value for this input parameter. If the input parameter was not found, *input_values* returns a *NaN*. If exactly one input was found, *input_values* will be that value. If several were found, *input_values* will be a cell array.
        3. asyncs = createAsyncRequest(thisModel, asyncs, type, fullTargetID, targetProperty, targetValue)
                   Creates an asynchronous request to mosaik that will be answered between this and the next iteration of the step function. *asyncs* is the list of map requests (can contain the map containers *getRequests* und *setRequests*). *type* is a string, either *get* or *set*. *fullTargetID* is the full ID (as a string) of the model the request addresses. It usually takes the form: Sim ID.Model ID. *targetProperty* is the Property (string) to be requested or modified. *targetValue* is an optional parameter that is only used if type == *set*. It specifies the new value for the *targetProperty*.


Java
====

In this section, the Java classes which expand the `mosaik Java API`_ are presented in order to form the java-side of the mosaik-matlab API.
Therefore, this section serves only to better understand the underlying processes. When using the mosaik-matlab API, no modifications in JAVA should be necessary at all.

.. _mosaik Java API: https://gitlab.com/mosaik/mosaik-api-java

MatlabSimulator
---------------
This is a Java class inheriting from the Java-APIs *Simulator* class and calls the MATLAB 
*Simulator* class.
This class basically translates the Java-API methods to the respective mosaik-matlab API methods.
Its class properties and methods are listed and briefly commented on in the following tables.

Properties:

=============   ======================= =================================================
Property Name   Type                    Comment
=============   ======================= =================================================
matlab          private MatlabProxy     matlabcontrol proxy used to communicate to matlab
simName         pricate String          Simulator name string
meta            private JSONObject      The simulators *meta* information.
                                        Communicates the initialization parameters,
                                        available models and parameters.
=============   ======================= =================================================

Methods:

===============   ======================= ============================= =================================================
Method Name       Return Type             Parameters                    Comment
===============   ======================= ============================= =================================================
MatlabSimulator   MatlabSimulator         String *simName*              Constrtuctor, also connects to MATLAB
init              JSONObject              String *sid*,                 calls the initializion and returns *meta*
                                          JSONObject *simParams*
create            JSONArray <JSONObject>  Int *num*,                    creates *num* models using *modelParams* and
                                          String *model*,               returns a list of all created objects.
                                          JSONObject *modelparams* 
step              double                  double *time*, 
                                          JSONObject *inputs*           Calls step function and hadles *asyncronous requests*.
                                                                        Careful: matlabcontrol cannot transmit the *long* type. 
getData           JSONObject              JSONObject *requests*         Retrieves the values of the parameters in *requests*
                                                                        from the model.
Cleanup           ..                      ..                            Destructor, Disconnects from MATLAB.
===============   ======================= ============================= =================================================


**Sidenote - variable types:**

A careful consideration of the code will show that this class often deals with JSONObjects as a Map<String,Object> and JSONArrays as List<>. This is inherited from the Java-API *Simulator* base class and seems to work fine, even though the resulting conversion warnings have to be suppressed. Here, they are referred to as JSONObjects/Arrays.
