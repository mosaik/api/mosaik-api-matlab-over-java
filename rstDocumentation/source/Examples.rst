Example Scenarios
*****************

Example 1:  Electricity-meter-Simulator
=======================================

This is a minimal version of a functional simulator.
It was integrated into the mosaik-demo program and has only one model that simply sums up the electricity that was consumed by each connected entity.

ElectricMeter
-------------
model
^^^^^

Apart from setting properties and constructor, this model for an electricity-meter only modifies the *step* method.
The *step* method takes the current load of the connected entity and multiplies the value with the *step_size* to get the energy that the entity used in this time-step. Then it adds this value to the so far used energy. As there are no asynchronous requests made, the *asyncs_map* remains empty. ::

        classdef ElectricMeter < model
		
            %ElectricMeter 
            %   Simulator model for an electricity meter that integrates the used
            %   electric power of a connected entity over time.
			
            properties
                Energy %Integrated Energy
                Power %Current Power
                conn_entity
            end
            
            methods
                function obj = ElectricMeter(Eid, params, SimID)
                    obj@model(Eid, params, SimID);
                    obj.Energy = params.init_val;
                    obj.type = 'ElectricMeter';
                    obj.Power = 0;

                end
                
                function asyncs = step(obj, inputs, step_size, ~)
                    %fprintf('Step function of %s called \n', obj.Eid)
                    asyncs = containers.Map();

                        
                    try 
                        [~, Power] = obj.unravel_inputs(inputs, 'Power'); %check inputs for errors
                    catch
                        warning(['No Power-Input recieved by ' obj.Eid])
                        return
                    end
                                 
                    %actual calculation
                    obj.Power = Power;
                    obj.Energy = obj.Energy+(obj.Power*step_size);
					
		    %Define return value
		    recall_time = time + step_size;
					
                    %fprintf('Power at call end: %d \n', obj.Power)
					
                end
				
            end
			
        end



Simulator
^^^^^^^^^
As this is a very simple model, no modifications to the methods defined in the abstract *Simulator* class are necessary.
Therefore, this simulator needs only to define the *metastruct* parameter. ::

        classdef ElectricMeterSim < Simulator
		
            % ElectricMeterSim 
            % Matlab simulator for the "ElectricMeter"-models.
			
            properties
                
            end
            
            methods
                    %Concrete class Constructor
                    function obj = ElectricMeterSim(simName)
					
                    obj@Simulator(simName);
                    %obj@Simulator(simName);
					
                    meter_meta = struct(); %Define Metadata for counter-model
                    meter_meta.public = true;
                    meter_meta.params = {'init_val'};
                    meter_meta.attrs ={'started','conn_entity','Power','type','Eid','Energy'};
                    meter_meta.any_inputs = true;
					
                    obj.metastruct.models = struct('ElectricMeter',meter_meta);
                          
                    end
					
            end
			
        end



Integration into demo_meter.py
------------------------------
To avoid errors during the opening of new matlab-sessions, start MATLAB by 
modifying the python *main* () function as shown below. ::



        if __name__ == '__main__':
		
            no_mat_sessions = 1 #number of MATLAB sessions needed (= number of matlab simulators)
            debug_matlab = False #open matlab hidden (for quick running) or openly (for debugging)
            cwd = sim_config.get('ElectricMeterSim').get('cwd')  #matlab folder path
			
            if(debug_matlab):
                debug_param = "debug"
            else:
                debug_param = "nodebug"

            #calls the Script that calls the java class that preopens MATLAB, then waits for initialization to complete
            os.system('java -cp StartMatlab.jar de.offis.mosaik.api.matlab.StartMatlab ' + cwd + ' ' + str(no_mat_sessions) + ' ' + debug_param)
            print('Waiting for MATLAB to get ready')
            time.sleep(2)

            main()



Where StartMatlab.cmd is a cmd-file starting or reactivating one matlab-session for each simulator that uses the MATLAB API. The *cwd* parameter is the MATLAB-directory as specified in *sim_config*.

The simulator has to be specified in the *sim_config* variable.
A correct definition for the *ElectricMeterSim* simulator would be: ::



        sim_config = {
        #	... other simulators ...
            'MATZSimulator': {
                'cmd': 'java -jar MatlabSimulator.jar %(addr)s ElectricMeterSim',
                'cwd': '*path to mosaik-matlab*\\MATLAB-API-DEMO\\matlab-elements'
            },
        }




The *cwd* parameter sets the working directory for matlab. The *cmd* is the command line that starts the mosaik-matlab API, passing the socket address and the name of the desired MATLAB-simulator as arguments.


After this, the MATLAB-based simulator can be handled like any other simulator.
In the example, the following actions were performed in the *create_scenario* function: ::



        def create_scenario(world):
            # Start simulators
        #   ...starting other simulators...

            emSim = world.start('ElectricMeterSim', step_size=60)

            # Instantiate models
        #	...instantiating other models...
            em = emSim.ElectricMeter.create(houses.__len__(), init_val=0) #Creating an ElectricMeter for each house

            # Connect entities
        #	...connecting other stuff...
            connect_meter_to_houses(world, houses, em) #connect each meter to a house

            # Database
        #   ...setting up database...

            # Web visualization
        #    ...setting up simulators for WebVis....

            connect_many_to_one(world, em, vis_topo, 'Energy')
            webvis.set_etypes({
                'ElectricMeter': {
                    'cls': 'load',
                    'attr': 'Energy',
                    'unit': 'E [J]',
                    'default': 0,
                    'min': 0,
                    'max': 1000000000,
                },
            })
        def connect_meter_to_houses(world, houses, em):
            for n in range(0, houses.__len__()):
                world.connect(houses[n], em[n], ('P_out', 'Power'))



Note that for the web visualization to work, the *etype* specified for the model (here *ElectricMeter*) must correspond to the *type* property specified in the model class constructor and simulator metastruct.



Example 2: Generator, Battery and Fuel-cell
===========================================

In this scenario, a *Battery* communicates with a
*Generator* and a *Fuelcell*. It receives as input a
power-offer from both of them and charges/discharges
for the sum of those powers as much as it can. 
It then returns via *setData* how much of the power it could
use. 
However, the *Generator* model ignores this information. The *FuelCell* notices how much power was used and only delivers an according amount of power.
The *ChargeControl* model is simple and only knows on or off.
Its *may_produce* parameter was set via *setData*.
For demonstration, a *getData* call is also included here. The requested value *SoC* could also be transmitted via the regular inputs, i.e. mosaiks connect-mechanism.
A rule is set to ensure that the *FuelCell* should only charge the *Battery* if the *SoC* of the *Battery* is more than 20% smaller than that of the *FuelCell*.
Otherwise, the *Battery* shall charge the *Fuelcell*.

Because the *Battery* requests the *SoC* of the *FuelCell* 
via *getData* and wants the answer in the same step, we need to use the {switch iteration}-procedure in the *step* function.
The async call *getData* will be created in the first iteration, executed from java between iterations, and the answer can be used in iteration two.

.. figure:: /MatAPIdocstatic/Demoschema.jpg
    :width: 800
    :align: center
    :alt: Dataflow between models in this example-scenario
    
    A simple illustration of the dataflow between the models of this Scenario.

The links to the code of the demo models and simulators can be found in the table below.

.. list-table::
   :widths: 25 25 25
   :header-rows: 1

   * - Component
     - Model
     - Simulator
   * - Battery
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/Battery/Battery.m>`_ 
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/Battery/MatBatSim.m>`_
   * - Fuel Cell
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/FuelCell/FuelCell.m>`_
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/FuelCell/FCSimulator.m>`_
   * - Generator
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/RandomPowerGenerator/Generator.m>`_
     - `Link to the code <https://gitlab.com/mosaik/mosaik-api-matlab-over-java/-/blob/master/MATLAB-API-DEMO/matlab-elements/Example-Simulators/RandomPowerGenerator/RPGSim.m>`_

